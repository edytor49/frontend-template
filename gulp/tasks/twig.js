let gp = require('gulp-load-plugins') (),
    fs = require('fs'),
    path = require('path');

module.exports = function () {
  $.gulp.task('twig', () => {
    return $.gulp.src('./_src/template/pages/*.twig')
      .pipe(gp.plumber({errorHandler: gp.notify.onError("Error: <%= error.message %>")}))
      .pipe(gp.data(function( file ) {
        if (fs.existsSync('./_src/data/' + path.basename(file.path) + '.json')) {
          return JSON.parse(fs.readFileSync('./_src/data/' + path.basename(file.path) + '.json'));
        }
      }))
      .pipe(gp.twig())
      // .pipe(gp.plumber.stop())
      .pipe($.gulp.dest('./app/'))
      .on('end', $.browserSync.reload);
  });
};
