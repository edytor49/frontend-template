let gp = require('gulp-load-plugins') (),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    pngquant = require('imagemin-pngquant'),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream'),
    imgPATH = {
        "input": './_src/image/content/**/*.{png,jpg,gif,svg}',
        "ouput": "./app/image/"
    };

module.exports = function () {
  $.gulp.task('img:dev', () => {
    return $.gulp.src(imgPATH.input)
      .pipe($.gulp.dest(imgPATH.ouput));
  });

  $.gulp.task('img:build', () => {
    return $.gulp.src(imgPATH.input)
      .pipe(gp.cache(gp.imagemin([
          gp.imagemin.gifsicle({interlaced: true}),
          // gp.imagemin.jpegtran({progressive: true}),
          gp.imagemin.optipng({optimizationLevel: 3}),
          gp.imagemin.svgo(),
          imageminJpegRecompress(),
          pngquant()
      ], {
          verbose: true
      })))
      .pipe($.gulp.dest(imgPATH.ouput));
  });

  $.gulp.task('img:cssbuild', () => {
    return $.gulp.src('./_src/image/css/**/*.{png,jpg,gif,svg}')
      .pipe(gp.cache(gp.imagemin([
        gp.imagemin.gifsicle({interlaced: true}),
        // gp.imagemin.jpegtran({progressive: true}),
        gp.imagemin.optipng({optimizationLevel: 3}),
        gp.imagemin.svgo(),
        imageminJpegRecompress(),
        pngquant()
      ], {
        verbose: true
      })))
      .pipe($.gulp.dest('./app/stylesheet/image'));
  });

  $.gulp.task('img:sprite', () => {
    var spriteData = $.gulp.src('./_src/image/css/sprite/*.png').pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: '_sprite.scss',
      imgPath: 'image/sprite/sprite.png'
    }));
    var imgStream = spriteData.img
      .pipe($.gulp.dest('./app/stylesheet/image/sprite/'));

    var cssStream = spriteData.css
      .pipe($.gulp.dest('_src/stylesheet/utils/'));

    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream);
  });
};
