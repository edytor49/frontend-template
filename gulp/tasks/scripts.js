let gp = require('gulp-load-plugins') (),
    scriptsPATH = {
      "input": "./_src/javascript/",
      "ouput": "./app/javascript/"
    },
    libsPath = [
      'node_modules/jquery/dist/jquery.min.js',
      // 'node_modules/popper.js/dist/umd/popper.min.js',
      // 'node_modules/bootstrap/js/dist/util.js',
      // 'node_modules/bootstrap/js/dist/dropdown.js',
      // 'node_modules/bootstrap/dist/js/bootstrap.min.js',
      // 'node_modules/swiper/dist/js/swiper.min.js',
      // 'node_modules/owl.carousel/dist/owl.carousel.min.js',
      // 'node_modules/responsively-lazy/responsivelyLazy.min.js',
      // 'node_modules/lazysizes/plugins/bgset/ls.bgset.min.js',
      // 'node_modules/lazysizes/lazysizes.min.js',
      // 'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
      // 'node_modules/bootstrap-select/dist/js/i18n/defaults-ru_RU.min.js',
      // 'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
      scriptsPATH.input + '*.js'
    ],
    copyLibs = [
      // 'node_modules/lazysizes/plugins/bgset/ls.bgset.min.js',
      // 'node_modules/lazysizes/lazysizes.min.js',
    ];

module.exports = function () {

  $.gulp.task('js:dev', () => {
    return $.gulp.src(libsPath)
      .pipe(gp.concat('libs.min.js'))
      .pipe($.gulp.dest(scriptsPATH.ouput))
      .pipe($.browserSync.reload({
        stream: true
      }));
  });

  $.gulp.task('js:build', () => {
    return $.gulp.src(libsPath)
      .pipe(gp.concat('libs.min.js'))
      .pipe(gp.uglify())
      .pipe($.gulp.dest(scriptsPATH.ouput))
  });

  // просто переместим необходимые библиотеки в папку со скриптами
  $.gulp.task('js:copy', () => {
    return $.gulp.src(copyLibs)
      .pipe($.gulp.dest(scriptsPATH.ouput))
  });
};
