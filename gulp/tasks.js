module.exports = [
  './gulp/tasks/clean',
  './gulp/tasks/fonts',
  './gulp/tasks/image',
  './gulp/tasks/svg',
  './gulp/tasks/styles',
  './gulp/tasks/scripts',
  // './gulp/tasks/pug',
  './gulp/tasks/twig',
  './gulp/tasks/server',
  './gulp/tasks/watch'
];
