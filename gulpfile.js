"use strict";

global.$ = {
  path: {
      task: require('./gulp/tasks.js')
  },
  gulp: require('gulp'),
  browserSync: require('browser-sync').create(),
  del: require('del')
};

$.path.task.forEach(function (taskPath) {
  require(taskPath)();
});

$.gulp.task('dev', $.gulp.series(
  'clean',
  $.gulp.parallel(
    // 'pug',
    'twig', // compile twig to html
    'fonts', // copy fonts
    'styles:dev', // minify & copy styles
    'img:dev', // minify & copy imgs
    'img:cssbuild', // minify & copy imgs that used in styles
    // 'img:sprite',
    'js:dev', // minify & copy js
    // 'js:copy',
    'svg' // minify & create svg sprite
  )
));

$.gulp.task('build', $.gulp.series(
  'clean',
  $.gulp.parallel(
    // 'pug',
    'twig',
    'fonts',
    'styles:build-min',
    'img:build',
    'img:cssbuild',
    // 'img:sprite',
    'js:build',
    'js:copy',
    'svg'
  )
));
$.gulp.task('default', $.gulp.series(
  'dev',
  $.gulp.parallel(
    'watch',
    'server'
  )
));
