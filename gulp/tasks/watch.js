module.exports = function () {
  $.gulp.task('watch', function () {
    // $.gulp.watch('./_src/template/**/*.pug', $.gulp.series('pug'));
    $.gulp.watch(['./_src/template/**/*.twig', './_src/data/**/*.twig.json'], $.gulp.series('twig'));
    $.gulp.watch('./_src/stylesheet/**/*.scss', $.gulp.series('styles:dev'));
    $.gulp.watch(['./_src/image/css/**/*.{png,jpg,gif}'], $.gulp.series('img:cssbuild'));
    $.gulp.watch(['./_src/image/content/**/*.{png,jpg,gif}'], $.gulp.series('img:build'));
    // $.gulp.watch(['./_src/image/content/sprite/*.png'], $.gulp.series('img:sprite'));
    $.gulp.watch('./_src/image/svg/*.svg', $.gulp.series('svg'));
    $.gulp.watch('./gulp/tasks/scripts.js', $.gulp.series('js:dev')); // !
    $.gulp.watch('./_src/javascript/**/*.js', $.gulp.series('js:dev'));
  });
};
