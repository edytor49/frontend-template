module.exports = function () {
  $.gulp.task('fonts', () => {
    return $.gulp.src('./_src/fonts/**/*.*')
      .pipe($.gulp.dest('./app/stylesheet/fonts/'));
  });
};
