let gp = require('gulp-load-plugins') (),
    bourbon = require('bourbon').includePaths,
    stylesPATH = {
      "input": "./_src/stylesheet/",
      "ouput": "./app/stylesheet/"
    };

module.exports = function () {
  $.gulp.task('styles:dev', () => {
    return $.gulp.src(stylesPATH.input + 'stylesheet.scss')
      .pipe(gp.plumber({errorHandler: gp.notify.onError("Error: <%= error.message %>")}))
      .pipe(gp.sourcemaps.init())
      .pipe(gp.sass({
        outputStyle: 'expanded',
        sourcemaps: true,
        includePaths: [bourbon]
      }))
      .pipe(gp.autoprefixer())
      .pipe(gp.sourcemaps.write())
      .pipe(gp.rename({ suffix: '.min', prefix : '' }))
      .pipe(gp.plumber.stop())
      .pipe($.gulp.dest(stylesPATH.ouput))
      .on('end', $.browserSync.reload);
  });
  $.gulp.task('styles:build', () => {
    return $.gulp.src(stylesPATH.input + 'stylesheet.scss')
      .pipe(gp.sass({
        outputStyle: 'expanded',
        sourcemaps: true,
        includePaths: [bourbon]
      }))
      .pipe(gp.autoprefixer())
      .pipe(gp.csscomb())
      .pipe($.gulp.dest(stylesPATH.ouput))
  });
  $.gulp.task('styles:build-min', () => {
    return $.gulp.src(stylesPATH.input + 'stylesheet.scss')
      .pipe(gp.sass({
        outputStyle: 'expanded',
        sourcemaps: true,
        includePaths: [bourbon]
      }))
      .pipe(gp.autoprefixer())
      .pipe(gp.csso())
      .pipe(gp.rename({ suffix: '.min', prefix : '' }))
      .pipe($.gulp.dest(stylesPATH.ouput))
  });
};
