# Frontend Template

Startup front-end template for building fast, robust, and adaptable web apps or sites.
Include jQuery, Bourbon.

* Version: 1.2
* Author: ed49 (edytor49@gmail.com)

## Quick start

1. Clone the git repo — `git clone https://edytor49@bitbucket.org/edytor49/frontend-template.git` or Download
2. install all dev packages - `yarn install` or `yarn`
3. run gulp - `gulp`

## Browser support

* Chrome *(latest 2)*
* Edge *(latest 2)*
* Firefox *(latest 2)*
* Internet Explorer 11
* Opera *(latest 2)*
* Safari *(latest 2)*

## License

The code is available under the MIT license

## Contributing

* Yarn - менеджер зависимотей (Подробнее: https://yarnpkg.com/en/ Миграция с NPM: https://yarnpkg.com/en/docs/migrating-from-npm)
* Gulp - сборщик проекта
* Sass - препроцессор CSS
* Pug (jade) - препроцессор HTML и шаблонизатор (https://pugjs.org/api/getting-started.html) (https://habrahabr.ru/post/278109/)
* Bourbon -- библиотека кросбраузерных миксинов

## Features

* BEM (только подход к наименованию классов)
* Архитектура sass стилей проекта, смесь SMACSS (Шаблон 7-1) Гайдлайн который поможет разобратся в структуре проекта тут: http://sass-guidelin.es/
* Framework: Bootstrap 4 (http://getbootstrap.com/) or UiKit 3 (https://getuikit.com/)
* Flexbox & Grid CSS

## Advance

* `yarn outdated` -- check last version for packages
* `yarn upgrade` or `yarn global upgrade` -- for upgrade all packages
* Персонализировать под проект favicon
* для итеграции Bootstrap 4 - `yarn add bootstrap, popper.js` and uncomment in stylesheet.scss
* для итеграции Font Awesome 4.7.0 - `yarn add font-awesome`, copy fonts files to _src/fonts/ dir and uncomment in stylesheet.scss

### Инструкция по инициализации проекта:

* Скачать последнюю версию template из Bitbucket используя для навигации теги
* Скопировать содержимое в корень проекта
* Установить зависимости проекта для разработки через пакетный менеджер yarn
* yarn install (or yarn) в консоли
* yarn outdated -- проверка наличия обновлений для используемых зависимостей
* yarn upgrade (yarn global upgrade) -- обновление используемых зависимостей к последней актуальной версии
* yarn add [package]@version] -- для установки пакетов, скриптов в папку node_modules
* в случае если предпочитаете использовать Bower для подключения скриптов:
* bower init -- для создания bower.json
* bower install -- установка vendor скриптов от которых зависит проект (к примеру: jQuery)
* gulp build -- запуск разработки проэкта
* gulp -- запуск сборщика проекта, а также watcher-а

### Links
* https://github.com/h5bp/html5-boilerplate - html template, Apache & Nginx configs
* https://github.com/agragregra/OptimizedHTML-5 - Lightweight startup HTML5 template, based on Gulp
* https://github.com/FARCER/Start-template-gulp-4-pug-sass/ - Стартовый шаблон автора канала FrontCoder
* https://gist.github.com/ZeRRoCull/6cfd9063b9e1db61703dc98549db0509 - Шаблон gulpfile с деплоем для Opencart
