let gp = require('gulp-load-plugins') ();

module.exports = function () {
  $.gulp.task('pug', () => {
    return $.gulp.src('./_src/template/pages/*.pug')
      .pipe(gp.plumber({errorHandler: gp.notify.onError("Error: <%= error.message %>")}))
      .pipe(gp.pug({ pretty: true }))
      .pipe(gp.plumber.stop())
      .pipe($.gulp.dest('./app/'))
      .on('end', $.browserSync.reload);
  });
};
